exports.login = (req, res) => {
  const creds = req.body;
  // console.log(req.body);
  const def_auth_cred = {
    email: "testuser@email.com",
    password: '123Qwe'
  };

  const def_user_info = {
    email: 'testuser@email.com',
    fName: 'John',
    lName: 'Doe',
    fav_team_id: "137705",
    fav_team_name: "Gamba Osaka"
  }

  if (def_auth_cred.email === creds.email.toLowerCase() && def_auth_cred.password === creds.password) {
    res.json({ message: 'Authentication Successful', data: def_user_info });
  } else {
    res.status(401).json({ error: "Authentication failed", message: "Invalid credentials" });
  }
}

