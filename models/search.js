const axios = require('axios');
const CircularJSON = require('circular-json');

const constants = require('../constants').constants;

let search = (req, res) => {
  const searchKey = req.query.search;
  const queryUrl1 = constants.sports_api + 'searchteams.php?t=' + searchKey;
  const queryUrl2 = constants.sports_api + 'searchplayers.php?p=' + searchKey;
  const queryUrl3 = constants.sports_api + 'searchevents.php?e=' + searchKey;
  let query_responses = {};

  if (!searchKey || searchKey == "") res.status(400).json({ error: "Invalid request", message: "Invalid search parameters" });
  axios.all([
    axios.get(queryUrl1),
    axios.get(queryUrl2),
    axios.get(queryUrl3)
  ]).then(axios.spread((res1, res2, res3) => {
    // console.log(res1, res2, res3);
    query_responses["teams"] = res1.data.teams;
    query_responses["players"] = res2.data.player;
    query_responses["events"] = res3.data.event;
    res.send({ message: 'Success', data: query_responses });
  })).catch(error => {
    res.status(500).json({ error: "Server Error", message: "Something went wrong, Please try again", data: error })
  });
}

module.exports = { search: search }
// https://www.thesportsdb.com/api/v1/json/1/searchevents.php?e=Arsenal_vs_Chelsea