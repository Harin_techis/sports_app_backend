const axios = require('axios');
const CircularJSON = require('circular-json');

const constants = require('../constants').constants;

let last_5_events = (req, res) => {
  const team_id = req.query.team_id;
  const queryUrl = constants.sports_api + 'eventslast.php?id=' + team_id;

  if (!team_id || team_id == "") res.status(400).json({ error: "Invalid request", message: "Invalid team_id" });
  axios.get(queryUrl)
    .then(response => {
      res.send({ message: 'Success', data: response.data })
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({ error: "Server Error", message: "Something went wrong, Please try again" })
    });
}

module.exports = { last_5_events: last_5_events }