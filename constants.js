let constants = {
  sports_api: 'https://www.thesportsdb.com/api/v1/json/1/',
}

module.exports = { constants: constants }