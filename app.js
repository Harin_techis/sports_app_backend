const express = require('express')
const ejs = require('ejs');
var cors = require('cors')
const bodyParser = require('body-parser');

const api = require('./routes/api');
const auth = require('./routes/auth');

const app = express()
app.use(cors())
app.use(bodyParser.json()); // application/json
app.use('/api', api);
app.use('/auth', auth);

app.get('/check_server', (req, res) => {
  res.send("OK");
})

app.get('**', (req, res) => {
  res.status(404).json({ error: "404 Resourse not found", message: "Resource not found" })
})

app.listen(3000);