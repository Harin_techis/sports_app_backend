const express = require('express');
const router = express.Router();

const auth = require('../models/auth');

// GET /feed/posts
router.post('/login', auth.login);

module.exports = router;
