const express = require('express');
const router = express.Router();

const search = require('../models/search');
const endpoints = require('../models/endpoints');

// Search teams & Players by name
router.get('/search', search.search);

// GET last 5 events of a team
router.get('/last_5_events', endpoints.last_5_events);

module.exports = router;
